// annotate all what is what


#include <iostream>
#include <winsock2.h>


using namespace std;

int main()
{
    WSADATA WSAData;

    SOCKET serverSock, clientSock;

    SOCKADDR_IN serverAddr, clientAddr;


    int nRet =0;

    WSAStartup(MAKEWORD(2,0), &WSAData);

    // add code here for TCP serversocket
    serverSock = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSock < 0){
        cout<<endl<<"fail";
    }else{
        cout<<endl<<"pass";}


    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    // add code here for ip family
    serverAddr.sin_port = htons(5555);

    bind(serverSock, (SOCKADDR *)&serverAddr, sizeof(serverAddr));
    listen(serverSock, 0);
    cout << "Listening for incoming connections..." << endl;

    char buffer[1024];
    int clientAddrSize = sizeof(clientAddr);
    if((clientSock = accept(serverSock, (SOCKADDR *)&clientAddr, &clientAddrSize)) != INVALID_SOCKET)
    {
        cout << "Client connected!" << endl;
        recv(clientSock, buffer, sizeof(buffer), 0);
        cout << "Client says: " << buffer << endl;
        memset(buffer, 0, sizeof(buffer));

        closesocket(clientSock);
        cout << "Client disconnected." << endl;
    }
}

